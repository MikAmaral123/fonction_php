<!DOCTYPE html>
<html class="background">
<head>
	<!--LINK-->
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">

	<!--META-->
	<meta charset="utf-8">

	<!--TITLE-->
	<title>Mon CV</title>
</head>

<!--BODY-->
<body>

	<div class="Title">
		<h1>CV</h1>
			<div class="container">
				<?php
					require "cv_data.php"
				?>
			</div>
	</div>

</body>
</html>