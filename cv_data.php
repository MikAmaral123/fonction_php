<?php

//COORDONNEES

$coordonnees = [
    'Prenom' => 'Mickael',
    'Nom' => 'Amaral',
    'Age' => '19',
    'Adresse' => '15 rue Jean Gabriel Milhas',
    'Ville' => 'Muret <br/>'];

//SCOLARITE

$scolarite = [
	[

	'Annee' => '2015',
	'Diplome' => 'Brevet des colleges',
	'Lieux' => 'Castelnaudary <br/>',
	],
	[

	'Annee' => '2017',
	'Diplome' => 'BEP commerce',
	'Lieux' => 'Castelnaudary <br/>',
	],
	[

	'Annee' => '2019',
	'Diplome' => 'BAC commerce',
	'Lieux' => 'Castelnaudary <br/>',
	]
			];

//EXPERIENCE

$experience = [
	[

	'Annee' => '2015',
	'entreprise' => 'Bricomarche',
	'Lieux' => 'Revel <br/>',
	],
	[

	'Annee' => '2016',
	'entreprise' => 'Micromania',
	'Lieux' => 'Blama <br/>',
	],
	[

	'Annee' => '2017',
	'entreprise' => 'Fnac',
	'Lieux' => 'Toulouse <br/>',
	]
			];

//LOISIRS

$loisirs = ['<br/>Codage', 'Vélo', 'Musculation', 'Montage vidéo', 'Photo'];

//FOREACH

echo "Information Personnel :<br/>";

foreach($coordonnees as $cle => $element)
{
    echo '' . $cle . ' : ' . $element . '<br />';
}

echo "Diplome :<br/>";

foreach ($scolarite as $element)
{
	foreach ($element as $cle => $info)

		echo '' . $cle . ' : ' . $info . '<br />'; 
}

echo "Experience Professionnel :<br/>";

foreach ($experience as $element)
{
	foreach ($element as $cle => $info)

		echo '' . $cle . ' : ' . $info . '<br />'; 
}

echo "Loisirs :<br/>";

foreach ($loisirs as $cle => $value) {
	echo $value . '<br />'; 
}

?>